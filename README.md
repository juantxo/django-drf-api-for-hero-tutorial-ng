In order to use the Hero-Tour-API, follow this steps:

    1) Download/clone the repo:
        $ git clone https://gitlab.com/juantxo/django-drf-api-for-hero-tutorial-ng.git
    
    2) Generate the database:
        $ python3 manage.py migrate
        
    3) Run the server:
        $python3 manage.py runserver

Now you have a functional API to use while you are doing the Angular Hero Tour Tutorial

**) In order to user the API admin panel go to:
        -> http://localhost:8000/api/v1/