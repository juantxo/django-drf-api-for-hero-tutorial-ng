from django.urls import path, include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'heroes', views.HeroViewSet)

urlpatterns = [
    #Heroes url views
    path('', include(router.urls)),
]
