from django.db.models import Max

from django.http import Http404
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework import viewsets
from .serializers import HeroSerializer
from .models import Hero


class HeroViewSet(viewsets.ModelViewSet):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer


    @action(methods=['GET'], detail=False)
    def search_hero(self, request):
        searched_hero = self.request.query_params.get('name', None)
        if searched_hero is not None:
            queryset = Hero.objects.filter(identity__icontains=searched_hero)
            serializer = HeroSerializer(queryset, many=True)
            if serializer:
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response('?name= parameter required.', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['GET'], detail=False)
    def get_last_hero_id(self, request):
        queryset = Hero.objects.all()
        hero_last_id = queryset.aggregate(Max('id'))
        last_id = hero_last_id['id__max']
        return Response(last_id, status=status.HTTP_200_OK)


    # @action(methods=['PUT', 'GET'], detail=False)
    # def update(self, request):
    #     params_to_update = {}
    #     for i in self.request.query_params.items():
    #         params_to_update += i
    #     return Response(params_to_update, status=status.HTTP_400_BAD_REQUEST)

