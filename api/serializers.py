from .models import Hero
from rest_framework import serializers


class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = ('id', 'identity', 'first_name', 'last_name', 'birthday', 'rating')
        model = Hero

