from django.db import models


class Hero(models.Model):
    identity = models.CharField(max_length=255, null=False, unique=True)
    first_name = models.CharField(max_length=255,null=False)
    last_name = models.CharField(max_length=255, null=False)
    birthday = models.DateField()
    rating = models.IntegerField()
